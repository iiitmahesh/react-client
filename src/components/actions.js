import React from 'react';
import { withRouter } from "react-router-dom";


class Actions extends React.Component{


  delete =() =>{
    // this.props.history.push("/form/")
      fetch("http://localhost:8080/api/movie/"+this.props.data._id,{method:"delete"})
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          // this.props.history.push("/list")
          window.location.reload()
          
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

   edit =(id) =>{
    this.props.history.push("/form/"+this.props.data._id)

  }

  render(){
    console.log(this.props.data)
      return (
    <div>
    <button  type="button" className="btn btn-primary" onClick={this.edit}>edit</button>
    <button  type="button" className="btn btn-primary" onClick={this.delete}>delete</button>
    </div>
  );
  }
}
export default withRouter(Actions)
