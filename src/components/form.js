import React from "react";
import { withRouter } from "react-router-dom";
import _ from "lodash";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      formData: {},
      type: "new"
    };
  }

  handleChange = event => {
    let formData = this.state.formData;

    _.set(formData, event.target.name, event.target.value);

    this.setState({ formData });
  };

  handleSubmit = event => {
    event.preventDefault();

    // alert('A name was submitted: ' + data);
    // this.postData(this.state.formData,this.state.type)
    let url = "http://localhost:8080/api/movie";
    let method = "post";
    let data = this.state.formData;
    let type = this.state.type;
    if (type === "edit") {
      url = "http://localhost:8080/api/movie/" + this.state.formData._id;
      method = "put";
    }
    fetch(url, {
      method: method,
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(
        result => {
          this.props.history.push("/list");

          console.log(result);
          this.setState({
            isLoaded: true,
            items: result.payload
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  componentWillMount() {}
  componentDidMount() {
    this.getData();
  }

  fetchData = id => {
    fetch("http://localhost:8080/api/movie/" + id)
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          this.setState({
            isLoaded: true,
            formData: result.payload
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  getData = () => {
    const params = this.props.match.params;
    const id = params.id ? params.id : "new";
    console.log(id);
    if (id && id === "new") {
      // this.props.newJob();
    } else {
      this.setState({ type: "edit" });
      this.fetchData(id);
    }
  };

  goToList = () => {
    this.props.history.push("/list");
  };

  render() {
    const { formData } = this.state;

    return (
      <div>
        <button
          type="button"
          className="btn btn-outline-primary list-movies-btn"
          onClick={this.goToList}
        >
          {" "}
          Go back to Movies{" "}
        </button>
        <div className="form-movie">
          <form onSubmit={this.handleSubmit} method="POST">
            <label>
              Name:
              <input
                type="text"
                name="name"
                value={formData.name ? formData.name : ""}
                onChange={this.handleChange}
              />
            </label>
            <br />
            <label>
              Type:
              <input
                type="text"
                name="type"
                value={formData.type ? formData.type : ""}
                onChange={this.handleChange}
              />
            </label>
            <br />
            <label>
              Rating:
              <input
                type="text"
                name="rating"
                value={formData.rating ? formData.rating : ""}
                onChange={this.handleChange}
              />
            </label>
            <br />
            <label>
              favorite:
              <input
                type="text"
                name="favorite"
                value={formData.favorite ? formData.favorite : ""}
                onChange={this.handleChange}
              />
            </label>
            <br />
            <input type="submit" value="Submit" />
          </form>
        </div>
      </div>
    );
  }
}
export default withRouter(Form);
