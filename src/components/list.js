
import React from 'react';
import { withRouter } from "react-router-dom";
import Actions from './actions'

var DataTable = require('react-data-components').DataTable;
require('react-data-components/css/table-twbs.css')


var columns = [
  { title: 'Name', prop: 'name'  },
  { title: 'Type', prop: 'type' },
  { title: 'Rating', prop: 'rating' },
  { title: 'Status', prop: 'status' },
  { title: 'Actions', prop: '_id',render: function(val, row) { return <Actions data={row}/> ; } },

];

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/movies/getAll")
      .then(res => res.json())
      .then(
        (result) => {
        	console.log(result)
          
          this.setState({
            isLoaded: true,
            items: result.payload
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  goToForm = () =>{
    this.props.history.push("/form/new")
  }

  render() {
    // console.log(this.props.history,"this.props.history")
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div>
        <button  type="button" className="btn btn-primary add-movie-btn" onClick={this.goToForm}> Add Movie </button>
          <DataTable
		      className="container"
		      keys={[ 'name', 'type' ]}
		      columns={columns}
		      initialData={this.state.items}
		      initialPageLength={15}
		      initialSortBy={{ prop: 'name', order: 'descending' }}
		      pageLengthOptions={[ 5, 15,25, 50 ]}
		    />
        </div>
      );
    }
  }
}
export default withRouter(List)