
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import List from './list'
import List2 from './list2'
import Form from './form'


class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
  }

  render() {
   return (
    <Router>
      <div>
        <Switch>
          <Route path="/list">
            <List2 />
          </Route>
          <Route path="/list-old">
            <List />
          </Route>
          <Route path="/form/:id">
            <Form />
          </Route>
          <Route path="/">
            <List2 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
}
export default Content