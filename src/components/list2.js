import React from "react";
import { withRouter } from "react-router-dom";
// import Actions from "./actions";
import MaterialTable from "material-table";

import { forwardRef } from "react";

import AddBox from "@material-ui/icons/AddBox";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

var columns = [
  { title: "Name", field: "name" },
  { title: "Type", field: "type" },
  { title: "Rating", field: "rating" },
  { title: "Status", field: "status" }
  // { title: 'Actions', field: '_id',render: function(val, row) { return <Actions data={row}/> ; } },
];

class List2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/movies/getAll")
      .then(res => res.json())
      .then(
        result => {
          // console.log(result);

          this.setState({
            isLoaded: true,
            items: result.payload
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

    deleteData =(data) =>{
      console.log(data)
    // this.props.history.push("/form/")
      fetch("http://localhost:8080/api/movie/"+data._id,{method:"delete"})
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          // this.props.history.push("/list")
          // window.location.reload()
          
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

   updateData =(data,type) =>{
    let url = "http://localhost:8080/api/movie";
    let method = "post";
    if(type === 'new'){
     data.status = "ACTIVE";
    }
    if (type === "edit") {
      url = "http://localhost:8080/api/movie/" + data._id;
      method = "put";
    }
    fetch(url, {
      method: method,
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(
        result => {
          this.props.history.push("/list");

          console.log(result);
          this.setState({
            isLoaded: true,
            items: result.payload
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
    // this.props.history.push("/form/"+this.props.data._id)

  }

  goToForm = () => {
    this.props.history.push("/form/new");
  };

  render() {
    // console.log(this.props.history,"this.props.history")
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div>
          <button
            type="button"
            className="btn btn-primary add-movie-btn"
            onClick={this.goToForm}
          >
            {" "}
            Add Movie{" "}
          </button>
          <MaterialTable
            columns={columns}
            icons={tableIcons}
            data={this.state.items?this.state.items:[]}
            title="Demo Title"
            editable={{
              onRowAdd: newData =>
                new Promise((resolve, reject) => {
                  setTimeout(() => {
                    {
                      const data = this.state.items;
                      data.push(newData);
                      this.setState({ data }, () => resolve());
                      this.updateData(newData,"new")

                    }
                    resolve();
                  }, 1000);
                }),
              onRowUpdate: (newData, oldData) =>
                new Promise((resolve, reject) => {
                  console.log(oldData,this.state.items.indexOf(oldData))
                  setTimeout(() => {
                    {
                      const data = this.state.items;
                      const index = data.indexOf(oldData);
                      data[index] = newData;
                      this.updateData(newData,"edit")
                      this.setState({ data }, () => resolve());
                    }
                    resolve();
                  }, 1000);
                }),
              onRowDelete: oldData =>
                new Promise((resolve, reject) => {
                  setTimeout(() => {
                    {
                      let data = this.state.items;
                      const index = data.indexOf(oldData);
                      data.splice(index, 1);
                      this.deleteData(oldData)
                      this.setState({ data }, () => resolve());
                    }
                    resolve();
                  }, 1000);
                })
            }}
          />
        </div>
      );
    }
  }
}
export default withRouter(List2);
